#pragma once
using namespace std;


class MyLib {

public:
	long long int removeEmptyDirs(string rootFolder);
	static void listFilesAndDirsInDir(string rootFolder, std::vector<string> *fileList, std::vector<string> *dirList);
	static  BOOL BrowseForFolder(HWND hwnd, LPCTSTR szCurrent, LPTSTR szPath);
};

