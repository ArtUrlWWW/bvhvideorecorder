// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>



// TODO: reference additional headers your program requires here
#include "screenCapture.h"
#include <psapi.h>

#include <time.h>
#include <stdlib.h>
#include <memory.h>

#include <fstream>
#include <string.h>

#include "BVH.h"

#include <math.h>
#include <gl/glut.h>


#include <windows.h>

#include "opencv2/opencv.hpp"


#include "boost\filesystem.hpp"
#include "boost\algorithm\string.hpp"

#include "Shlobj.h"

#include "MyLib.h"


