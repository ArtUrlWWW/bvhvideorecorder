#include "stdafx.h"	



IplImage makeScreenShot(std::vector<HWND> &list) {
	WindowCapture ws(list[0], true);
	cv::Mat m;
	ws.captureFrame(m);

	cv::Mat m2;
	cvtColor(m, m2, CV_BGRA2BGR);
	IplImage img = m2;
	return img;
}

void listDir() {
	using namespace boost::filesystem;
	//	recursive_directory_iterator dir(search_here), end;
}

long long int MyLib::removeEmptyDirs(string rootFolder) {
	long long int folderSize = 0;
	boost::replace_all(rootFolder, "\\\\", "\\");
	boost::filesystem::path folderPath(rootFolder);
	if (boost::filesystem::exists(folderPath)) {
		boost::filesystem::directory_iterator end_itr;

		for (boost::filesystem::directory_iterator dirIte(rootFolder); dirIte != end_itr; ++dirIte)
		{
			boost::filesystem::path filePath(dirIte->path());
			try {
				if (!boost::filesystem::is_directory(dirIte->status()))
				{

					folderSize = folderSize + boost::filesystem::file_size(filePath);
					cout << folderSize << endl;
				}
				else {
					folderSize = folderSize + removeEmptyDirs(filePath.string());
				}
			}
			catch (exception& e) {
				cout << e.what() << endl;
			}
		}
		if (folderSize == 0) {
			try {
				if (boost::filesystem::remove_all(rootFolder)) {
					//	MessageBox(nullptr, L"remove_all!", L"CREATED!", MB_OK);
				}
				else {
					//	MessageBox(nullptr, L"NOT remove_all!", L"NOT CREATED!", MB_OK);
				}
			}
			catch (boost::filesystem::filesystem_error) {
				//	MessageBox(nullptr, L"EXCEPTION!", L"NOT CREATED!", MB_OK);
			}
		}
	}
	return folderSize;
}

void MyLib::listFilesAndDirsInDir(string rootFolder, std::vector<string> *fileList, std::vector<string> *dirList) {
	boost::replace_all(rootFolder, "\\\\", "\\");
	boost::filesystem::path folderPath(rootFolder);
	if (boost::filesystem::exists(folderPath)) {
		boost::filesystem::directory_iterator end_itr;

		for (boost::filesystem::directory_iterator dirIte(rootFolder); dirIte != end_itr; ++dirIte)
		{
			boost::filesystem::path filePath(dirIte->path());
			try {
				if (boost::filesystem::is_directory(dirIte->status()))
				{
					(*dirList).push_back(filePath.string());
					MyLib::listFilesAndDirsInDir(filePath.string(), fileList, dirList);

				}
				else {
					(*fileList).push_back(filePath.string());
					//folderSize = folderSize + boost::filesystem::file_size(filePath);

				}
			}
			catch (exception& e) {
				cout << e.what() << endl;
			}
		}

	}
}

//fileList.erase(std::remove(fileList.begin(), fileList.end(), s), fileList.end());

inline static int CALLBACK BrowseCallbackProc(HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData)
{
	// If the BFFM_INITIALIZED message is received
	// set the path to the start path.
	switch (uMsg)
	{
	case BFFM_INITIALIZED:
	{
		if (NULL != lpData)
		{
			SendMessage(hwnd, BFFM_SETSELECTION, TRUE, lpData);
		}
	}
	}
	return 0; // The function should always return 0.
}

/*--------------------------------------------------------------------*/
/*  BrowseForFolder                                                   */
/*--------------------------------------------------------------------*/
/**
*  @brief   open a folder dialog to choose folder
*
*  @param   HWND                 [in] is the parent window.
*           szCurrent            [in] is an optional start folder. Can be NULL.
*           szPath               [in] receives the selected path on success. Must be MAX_PATH characters in length.
*
*  @return  TRUE: open and return folder path which is choosed
*           FALSE: can't open
*/
 BOOL MyLib::BrowseForFolder(HWND hwnd, LPCTSTR szCurrent, LPTSTR szPath)
{

	BROWSEINFO   bi = { 0 };
	LPITEMIDLIST pidl;
	TCHAR        szDisplay[MAX_PATH];
	BOOL         retval;

	CoInitialize(NULL);
	if (szPath == NULL) {
		szPath = szDisplay;
	}
	bi.hwndOwner = hwnd;
	bi.pszDisplayName = szDisplay;
	bi.lpszTitle = TEXT("Select dir for indexing");
	bi.ulFlags = BIF_RETURNONLYFSDIRS | BIF_USENEWUI | BIF_NEWDIALOGSTYLE ;
	bi.lpfn = BrowseCallbackProc;
	bi.lParam = (LPARAM)szCurrent;
	// open browser folder dialog
	pidl = SHBrowseForFolder(&bi);
	if (NULL != pidl)
	{
		// get folder path
		retval = SHGetPathFromIDList(pidl, szPath);
		CoTaskMemFree(pidl);
	}
	else
	{
		retval = FALSE;
	}
	if (!retval)
	{
		szPath[0] = TEXT('\0');
	}
	CoUninitialize();
	return retval;
}