From http://www.oshita-lab.org/software/bvh/index.html

For first you need to compile Boost library - please, run file
ThirdParty/boost_1_60_0/RunMe.bat


For git:
git config --system core.longpaths true

For Boost, if you would like to compile it not with the file ThirdParty/boost_1_60_0/RunMe.bat :
1) Double click ThirdParty/boost_1_60_0/bootstrap.bat
2) Run  
ThirdParty/boost_1_60_0/bjam toolset=msvc-14.0 --build-type=complete --with-thread --with-chrono --with-date_time --with-filesystem --with-regex link=static variant=release runtime-link=static address-model=64 architecture=x86